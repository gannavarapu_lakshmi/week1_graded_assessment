package com.greatLearning.assignment;

public class Main {
	public static void main(String[] args) {
		SuperDepartment sd=new SuperDepartment();
		System.out.println("	SUPER_DEPARTMENT_DETAILS---->");
		System.out.println("Department_Name: "+sd.departmentName());
		System.out.println("Todays_Work: "+sd.getTodaysWork());
		System.out.println("Dead_Line: "+sd.getWorkDeadline());
		System.out.println("Today_Holiday: "+sd.isTodayHoliday());
		
		AdminDepartment ad=new AdminDepartment();
		System.out.println("	ADMIN_DEPARTMENT---->");
		System.out.println("Department_Name: "+ad.departmentName());
		System.out.println("Todays_Work: "+ad.getTodaysWork());
		System.out.println("Dead_Line: "+ad.getWorkDeadline());
		System.out.println("Today_Holiday: "+ad.isTodayHoliday());
	
		HrDepartment hd=new HrDepartment();
		System.out.println("	HR_DEPARTMENT_DETAILS----->");
		System.out.println("Department_Name: "+hd.departmentName());
		System.out.println("Todays_Work: "+hd.getTodaysWork());
		System.out.println("Dead_Line: "+hd.getWorkDeadline());
		System.out.println("Today_Holiday: "+hd.isTodayHoliday());
		System.out.println("Activity: "+hd.doActivity());
	
		TechDepartment td=new TechDepartment();
		System.out.println("	TECH_DEPARTMENT_DETAILS----->");
		System.out.println("Department_Name: "+td.departmentName());
		System.out.println("Todays_Work: "+td.getTodaysWork());
		System.out.println("Dead_Line: "+td.getWorkDeadline());
		System.out.println("Today_Holiday: "+td.isTodayHoliday());
		System.out.println("TechStackInfo: "+td.getTechStackInformation());
	}

}
